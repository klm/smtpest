unit u_smtpest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, IdSMTP, IdMessage, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls;

type

  { TSmtpest1 }

  TSmtpest1 = class(TForm)
    AuthMethodeSelect: TComboBox;
    AuthSupported: TButton;
    Aboutte: TButton;
    CheckBox1: TCheckBox;
    Infos: TLabel;
    Objet: TLabeledEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    ValideSaisieButton: TButton;
    Envoi: TButton;
    //Envoi.caption:='Balance la '#13'purée,'#13'coco !';
    IdMessage1: TIdMessage;
    IdSMTP1: TIdSMTP;
    Destinataire: TLabeledEdit;
    Expediteur: TLabeledEdit;
    AuthMethode: TLabel;
    Login: TLabeledEdit;
    MotDePasse: TLabeledEdit;
    Port: TLabeledEdit;
    MessageLabel: TLabel;
    Message: TMemo;
    Serveur: TLabeledEdit;
    procedure AboutteClick(Sender: TObject);
    procedure AuthMethodeSelectEditingDone(Sender: TObject);
    procedure AuthSupportedClick(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure EnvoiMessage(Sender: TObject);
    procedure ValideSaisie(Sender: TObject);

  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Smtpest1: TSmtpest1;

implementation

{$R *.frm}

{ TSmtpest1 }



  procedure TSmtpest1.ValideSaisie(sender: Tobject);
  begin
    IdMessage1.From.Address:=Expediteur.Text;

    IdMessage1.Body.Clear;
    IdMessage1.Body.AddStrings(Message.Lines.Text);

    IdMessage1.Recipients.Clear;
    IdMessage1.Recipients.Add.Address:=Destinataire.Text;

    IdMessage1.Subject:=Objet.Text;


    IdSMTP1.Host:=Serveur.Text;
    IdSMTP1.Port:=StrToInt(Port.Text);
    if AuthMethodeSelect.ItemIndex=1 then
    begin
      IdSMTP1.AuthType:=satDefault;
      IdSMTP1.Password:=MotDePasse.Text;
      IdSMTP1.Username:=Login.Text;
    end;
    if AuthMethodeSelect.ItemIndex=0 then
    begin
      IdSMTP1.AuthType:=satNone;
    end;
    Envoi.Enabled:=true;
  end;

  //PROCEDURE RECUP INFOS
  procedure TSmtpest1.AuthSupportedClick(Sender: TObject);
  begin
    IdSMTP1.Host:=Serveur.Text;
    IdSMTP1.Port:=StrToInt(Port.Text);
    IdSMTP1.Connect;
    ShowMessage('serveur: '+IdSMTP1.Host+#10#13+IdSMTP1.Capabilities.Text);
    IdSMTP1.Disconnect(true);

  end;

procedure TSmtpest1.CheckBox1Change(Sender: TObject);
begin
     case MotDePasse.PasswordChar of
       #0: MotDePasse.PasswordChar:='*';
       '*': MotDePasse.PasswordChar:=#0;
       //else MotDePasse.PasswordChar:='*';
     end;

  //if MotDePasse.PasswordChar=#0 then
  //   begin
  //   MotDePasse.PasswordChar:='*';
  //   end;
  //
  //   if MotDePasse.PasswordChar='*' then
  //   begin
  //   MotDePasse.PasswordChar:=#0;
  //   end;



end;

  //PROCEDURE ACTIVE AUTH METHODE
  procedure TSmtpest1.AuthMethodeSelectEditingDone(Sender: TObject);
  begin
    if AuthMethodeSelect.ItemIndex=1 then
    begin
      MotDePasse.Enabled:=true;
      Login.Enabled:=true;
    end;
    if AuthMethodeSelect.ItemIndex=0 then
    begin
      MotDePasse.Enabled:=false;
      Login.Enabled:=false;
    end;
    //ValideSaisie(sender);
  end;

procedure TSmtpest1.AboutteClick(Sender: TObject);
begin
  ShowMessage('Ce logiciel a été créé dans le but de tester rapidement des paramètres SMTP.' + sLineBreak + 'Et accessoirement, de récupérer quelques infos de config concernant le serveur cible.' + sLineBreak + 'Je n ai pas implémenté de résolution DNS, utilisez une IP exclusivement, pour le serveur.' + sLineBreak + 'Mael, "klmmlk".');
end;

  //PROVEDURE ENVOI MESSAGE
    procedure TSmtpest1.EnvoiMessage(Sender: TObject);
  begin
    try
      try
        IdSMTP1.Connect;
      except
        on E : Exception do
          ShowMessage(E.ClassName+'EnvoiMessage Connect Erreure. message : '+E.Message);
      end;
      try
         if AuthMethodeSelect.ItemIndex=1 then
          begin
            IdSMTP1.Password:=MotDePasse.Text;
            IdSMTP1.Username:=Login.Text;
          end;
        IdSMTP1.Send(IdMessage1);
      except
        on E : Exception do
          ShowMessage(E.ClassName+'EnvoiMessage Send Erreure. message : '+E.Message);
      end;
    finally
      try
      IdSMTP1.Disconnect(true);
      IdMessage1.Clear;
      ValideSaisie(sender);
      except
        on E : Exception do
          ShowMessage(E.ClassName+'EnvoiMessage Disconnect/Clear Erreure. message : '+E.Message);
      end;
      //ShowMessage('Fin EnvoiMessage');
    end;

  end;

end.

